#VQL Admin 

Este projeto é um boilerplate para um painel administrativo utilizando VueJS 2, Quasar Framework e Laravel 5.7.

Baseado no projeto:

[laravel-quasar-pwa-template](https://github.com/impxd/laravel-quasar-pwa-template)

## O que este boilerplate disponibiliza

* Layout com a seguinte estrutura:
    * Cabeçalho
    * Sidebar
    * Conteúdo (atualizado através do VueRouter)
    * Rodapé
* Sistema de notificação que monitora requisições ajax
* Sistema de autenticação e autorização
* Middlewares para rotas do VueRouter
* Cliente HTTP (axios) configurado (token de autorização e csrf token) disponível de forma global para todos os componentes
* Ajax bar integrado com rotas

## Requerimentos

* PHP >= 7.1
* Composer
* Git
* NodeJS >= 8.11
* Quasar CLI >= 0.17.15

## Procedimento de instalação e configuração

```
# instalar os pacotes do php
composer install

# criar um arquivo .env e configurar a conexão do banco
cp .env.example .env

# criar uma chave
php artisan key:generate

# criar tabelas do banco
php artisan migrate

# criar um usuário admin, permissões e perfil
php artisan db:seed

# criar um oauth2 keys
php artisan passport:keys

# rodar o passport install e migrate novamente?
# php artisan passport:install
# php artisan migrate

# criar um oauth2 client para o front-end
php artisan passport:client --password

# no arquivo .env, configurar o client id e secret
WEB_APP_CLIENT_ID=xxx
WEB_APP_CLIENT_SECRET=xxx

# instalar o quasar-cli (caso ainda não tenha sido instalado)
sudo npm -g install quasar-cli

# instalar os pacotes js
npm install

```
## Configuração

As variáveis de ambiente do backend estão no arquivo **.env**.

As variáveis do front-end estão no arquivo **src/store/app/state.js**.

Configurar (nome, descrição, etc.) os arquivos: **composer.json**, **package.json** e **[quasar.conf.js](https://quasar-framework.org/guide/app-quasar.conf.js.html)**.

## Comunicação entre Back-End e Front-End

Todas as requisições serão feitas via ajax e o servidor sempre responderá um JSON com a seguinte estrutura:

```
{
    sucesso: true,
    mensagem: "Salvo com sucesso",
    dados: null
}
```

Onde:

```
sucesso: bool
mensagem: string
dados: null|object|array|string
```

**Importante**

Sempre que o servidor retornar uma mensagem diferente de vazio, esta mensagem será exibida no front-end atravéis do serviço **$notify**.

## Desenvolvimento do Front-End e build para produção

Será necessário levantar dois serviços, um para o back-end, outro para o front-end.

Caso não tenha configurado o back-end no apache, utilizar o servidor do laravel: 

```
php artisan serve
# http://127.0.0.1:8000
```

Para levantar o serviço do front-end com hot reload, utilizar o comando:

```
quasar dev -m spa
# http://127.0.0.1:8080
```

Ou configurar o apache para o lado do servidor.

## Arquivos do Front-End

Os arquivos estão localizados na pasta **src**.

Alguns pontos importantes:

* O componente **App.vue** é o componente de entrada do sistema

* Os plugins em **/src/plugins** são carregados antes do **App.vue**

* Para carregar serviços (**/src/services**), foi adicionado o arquivo **/src/plugins/serviceProvider.js**.

* O arquivo onde o Vue é instanciado pode ser visto em "**.quasar/entry.js**"

## Build para produção

Para compilar os assets e gerar um build de produção, utilizar o comando:

```
quasar build -m spa
```

Após o deploy, serão publicados os seguintes arquivos/pastas:

* public/app/app.html
* public/app/fonts
* public/app/js
* public/app/statics
* public/app/app.xxx.css

O **app.html** será lido pelo laravel através da view **app.blade.php** e será adicionado um **csrf-token** ao **app.html**.

## Autenticação e sessão

A autenticação é uma implementação do Oauth2 utilizando o [Laravel Passport](https://laravel.com/docs/5.7/passport) utilizando um [Password Grant Token](https://laravel.com/docs/5.7/passport#password-grant-tokens).

Após o login do usuário, o front-end irá armazenar o access token e irá incluí-lo no cabeçalho (Authorization: Bearer xxx) de todas as requisições feitas no back-end.

Em todos as requisições do tipo POST, PATCH, PUT e DELETE será necessário enviar um csrf token no cabeçalho (X-Csrf-Token). Para esta finalidade, será necessário criar uma sessão baseada em Cookie para armazenar o token.

## Serviços

Para carregar serviços (**/src/services**), foi adicionado o arquivo **/src/plugins/serviceProvider.js**. 

Para criar um novo serviço, adicione um arquivo na pasta **/src/services** e configure o **serviceProvider.js**

### /src/services/auth.js

Fornece uma maneira simples de verificar a sessão e as permissões do usuário.

O serviço está disponível dentro dos componentes através da propriedade **$http**.

Os dados estão armazenados na store **/src/store/usuario**.

O serviço fornece ainda métodos para login/logout.

Exemplo: 
```
<template>
    <div>
        <ul v-if="$auth.check()">
            <li>Você está autenticado</li>
            <li v-if="$auth.perfilAdministrador()">E tem o perfil de administrador</li>
            <li v-if="$auth.perfil('administrador')">E tem o perfil de administrador</li>
            <li v-if="$auth.permissao('perfil.administrador')">E tem a permissão de administrador</li>
            <li>Tem o id: #{{ $auth.id() }}</li>
            <li>Tem o nome: {{ $auth.nome() }}</li>
            <li>E-mail através da store: {{ $store.state.usuario.email }}</li>
        </ul>
        <p v-else>Você não está autenticado</p>
    </div>
</template>
<script>
    export default {
        name: 'MeuComponente',
        created () {
            if (this.$auth.check()) {
                console.log('está autenticado')
            } else {
                console.log('não está autenticado')
            }
        },
        methods: {
            login () {
                this.$auth.login({
                    id: 1,    
                    nome: "Fulano",    
                    email: "fulano@hotmail.com",    
                    permissoes: ['perfil.usuario']    
                })
            },
            logout () {
                this.$auth.logout()
            }
        }
    }
</script>
```

### /src/services/http.js

Fornece o cliente HTTP **[Axios](https://github.com/axios/axios)** já confiugrado.

Ao carregar a aplicação, o serviço irá buscar um csrf token no servidor.

Depois irá adicionar a todas as requisições um cabeçalho **X-CSRF-TOKEN**.

O serviço irá então monitorar todas as respostas do servidor, e caso alguma contenha um cabeçalho **X-CSRF-TOKEN**, o token será atualizado automaticamente.

O serviço está disponível dentro dos componentes através da propriedade **$http**.

Exemplo: 

```
<template>
</template>
<script>
    export default {
        name: 'MeuComponente',
        created () {
            this.$http.get('/api/v1/consulta').then(() => {
                console.log('consultado!')
            })
        }
    }
</script>
```

### /src/services/middleware.js

Este serviço executa middlewares criados antes de executar as rotas.

Exemplo de rota (**/src/router/routes.js**):

```
{ 
    path: '/auth/perfil',
    name: 'auth.perfil',
    component: () => import('pages/auth/perfil'),
    meta: {middleware: ['auth', 'my-middleware']}
}
```

**Importante**

No arquivo de rotas, todas as rotas devem ter nome e o middleware presente, mesmo que vazio.

Exemplo de middleware:

```
import { CreateMiddleware } from './middleware.js'

const myMiddleware = function () {
    console.log('myMiddleware init')
    // para interromper e redirecionar para alguma rota
    throw {name: 'erro'}
    // para não interromper, basta não lançar a exceção
}

const myMiddlewareName = 'my-middleware'

CreateMiddleware(myMiddlewareName, myMiddleware)
```

**Importante**

Caso queira interromper a rota, lançar uma exceção, e caso queira redirecionar, a exceção deve ser um objeto com a propriedade "name" ou "route", que deve constar o nome da rota.

Para seguir o fluxo normal da rota, basta não lançar nenhuma exceção.

### /src/services/notify.js

Este serviço integra o **[Notify](https://quasar-framework.org/components/notify.html)** do Quasar, o Axios e as respostas do Back-End, automatizando as mensagens a serem exibidas ao usuário.

Por exemplo, caso o servidor responda:

```
{
    sucesso: true,
    mensagem: "Salvo com sucesso.",
    dados: null
}
```

Irá aparecer uma notificação no Front-End automaticamente com o conteúdo "Salvo com sucesso.".

Caso retorne "sucesso: false" ou um erro de conexão ou status de erro (500 por exemplo), o serviço írá exibir uma mensagem de erro. O conteúdo da mensagem vai depender da resposta do servidor. 

## Links

* [Laravel](https://laravel.com/docs/5.7)

* [VueJS](https://vuejs.org/)

* [Vue Router](https://router.vuejs.org/)

* [Vuex](https://vuex.vuejs.org/guide/)

* [Axios](https://github.com/axios/axios)

* [Axios Middleware](https://emileber.github.io/axios-middleware/#/)

* [Quasar Framework](https://quasar-framework.org/components/)

* [Vuelidate](https://monterail.github.io/vuelidate/)

* [https://color.adobe.com/pt/Tema-de-Color-3-color-theme-11181846/](https://color.adobe.com/pt/Tema-de-Color-3-color-theme-11181846/)

* [laravel-quasar-pwa-template](https://github.com/impxd/laravel-quasar-pwa-template)

## Observações

* Aparentemente há um bug no compontente **q-btn** que não aciona a rota através do atributo **to="/recurso"**. Utilizar a o método **routeTo** no evento **click**. Ex.: "**<q-btn @click="routeTo('/recurso')"></q-btn>**"
