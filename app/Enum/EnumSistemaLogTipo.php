<?php

namespace App\Enum;

trait EnumSistemaLogTipo
{
    /**
     * @var array
     */
    protected static $enumTipo = ['Log', 'Auditoria'];

    /**
     * @var array
     */
    public static function enumTipo()
    {
        return static::$enumTipo;
    }
}
