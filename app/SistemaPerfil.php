<?php

namespace App;

use App\Foundation\Model\BaseTimestampsSoftDeletesModel;
use App\SistemaPermissao;

class SistemaPerfil extends BaseTimestampsSoftDeletesModel
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissoes()
    {
        return $this->belongsToMany(SistemaPermissao::class, 'sistema_perfil_permissao');
    }
}
