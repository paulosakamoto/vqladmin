<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OauthAccessToken extends Model
{
    /**
     * @var string
     */
    public $table = 'oauth_access_tokens';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @param $jwt string
     * @return  static
     */
    public static function fromJWT($jwt)
    {
        $partials = explode('.', $jwt);
        $header = json_decode(base64_decode($partials[0]));
        if (empty($header->jti)) {
            throw new ModelNotFoundException;
        }
        $id = $header->jti;

        return static::findOrFail($id);
    } 
}
