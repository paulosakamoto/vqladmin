<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Foundation\Auth\Authenticatable;
use App\Foundation\Model\BaseTimestampsSoftDeletesModel;

class SistemaUsuario extends BaseTimestampsSoftDeletesModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, 
        Authorizable, 
        CanResetPassword, 
        MustVerifyEmail, 
        Notifiable, 
        HasApiTokens;

    /**
     * @var array
     */
    protected $fillable = [
        'nome', 'email', 'senha',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'senha', 'token_senha',
    ];

    /**
     * @var string
     */
    protected $rememberTokenName = 'token_senha';

    /**
     * @param $senha string
     * @return static
     */
    public function setSenhaAttribute($senha)
    {
        $senha = substr($senha, 0, 2) == '$2' ? $senha : bcrypt($senha);

        $this->attributes['senha'] = $senha;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->senha;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function perfis()
    {
        return $this->belongsToMany(SistemaPerfil::class, 'sistema_usuario_perfil');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function permissoes($columns = [])
    {
        $columns = !empty($columns) ? $columns : ['sistema_permissao.*'];

        $query = SistemaPermissao::distinct()
            ->join(
                'sistema_perfil_permissao',
                'sistema_permissao.id',
                '=', 
                'sistema_perfil_permissao.sistema_permissao_id'
            )
            ->join(
                'sistema_usuario_perfil',
                'sistema_usuario_perfil.sistema_perfil_id',
                '=', 
                'sistema_perfil_permissao.sistema_perfil_id'
            )

            ->where('sistema_usuario_perfil.sistema_usuario_id', '=', $this->getKey())
            ;

        return $query->get($columns);
    }
}
