<?php
namespace App\Http\Middleware;

use Closure;
use \Symfony\Component\HttpFoundation\Response;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowHeaders = [
            'Origin',
            'Content-Type',
            'Accept',
            'Authorization',
            'Access-Control-Allow-Origin',
            'X-Request-With',
            'X-Requested-With',
            'X-Data-Hash',
            'X-CSRF-TOKEN',
        ];

        $exposeHeaders = [
            'Cache-Control',
            'Content-Type',
            'Date',
            //'X-Request-With',
            'X-Msg',
            'X-Data-Hash',
            'X-Data-Count',
            'X-Total-Count',
            'X-Pagination-Count',
            'X-Pagination-Page',
            'X-Pagination-Pages',
            'X-Pagination-Page',
            'X-Pagination-Limit',
            'X-Pagination-From',
            'X-Pagination-To',
            'X-Powered-By',
            'X-CSRF-TOKEN',
        ];

        $allowMethods = 'GET, POST, PUT, DELETE, OPTIONS, PATCH';
        $allowHeaders = implode(', ', $allowHeaders);
        $exposeHeaders = implode(', ', $exposeHeaders);

        // para o desenvolvimento liberar tudo
        if (appInDevelopment()) {
            $origin = $request->header('Origin') ?: $request->header('Host');
            $allowOrigin = $origin;
            header('Access-Control-Allow-Credentials: true');
        } else {
            $allowOrigin = appDomain();
        }

        if ($request->method() === 'OPTIONS') {

            $response = new Response('', 200, [
                'Access-Control-Allow-Origin' => $allowOrigin,
                'Access-Control-Allow-Methods' => $allowMethods,
                'Access-Control-Allow-Headers' => $allowHeaders,
                //'Access-Control-Request-Method' => $allowHeaders,
                //'Access-Control-Allow-Credentials' => true,
            ]);

            return $response;
        }

        $response = $next($request);

        if (is_object($response) && $response instanceof Response) {
            $response->headers->set('Access-Control-Allow-Origin', $allowOrigin);
            $response->headers->set('Access-Control-Allow-Methods', $allowMethods);
            $response->headers->set('Access-Control-Expose-Headers', $exposeHeaders);
        }

        return $response;
    }
}
