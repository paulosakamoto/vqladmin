<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Session\DatabaseSessionHandler;
use Illuminate\Session\FileSessionHandler;
use Illuminate\Contracts\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class ApiStartSession extends StartSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->method() == 'OPTIONS') {
            return $next($request);
        }

        return parent::handle($request, $next);
    }

    /**
     * Get the session implementation from the manager.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Session\Session
     */
    public function getSession(Request $request)
    {
        $session = $this->manager->driver();
        $sessionId = $request->cookies->get($session->getName());

        $session->setid($sessionId);

        if ($sessionId) {
            if ($this->usingDatabaseSessions()) {
                $table = config('session.table') ?: 'sessions';
                $result = DB::table($table)->where('id', '=', $sessionId)->first();
                if (!$result) {
                    $session->regenerate();
                }
            } elseif ($this->usingFileSessions()) {
                $path = config('session.files') ?: storage_path('framework/sessions');
                $filename = $path.'/'.$sessionId;
                if (!file_exists($filename)) {
                    $session->regenerate();
                }
            }
        }

        return $session;
    }

    /**
     * Add the session cookie to the application response.
     *
     * @param  \Symfony\Component\HttpFoundation\Response  $response
     * @param  \Illuminate\Contracts\Session\Session  $session
     * @return void
     */
    protected function addCookieToResponse(Response $response, Session $session)
    {
        //if ($this->usingCookieSessions() || $this->usingDatabaseSessions()) {
            $this->manager->driver()->save();
        //}

        if ($this->sessionIsPersistent($config = $this->manager->getSessionConfig())) {
            $response->headers->setCookie(new Cookie(
                $session->getName(), $session->getId(), $this->getCookieExpirationDate(),
                $config['path'], $config['domain'], $config['secure'] ?? false,
                $config['http_only'] ?? true, false, $config['same_site'] ?? null
            ));
        }
    }

    /**
     * Determine if the session is using cookie sessions.
     *
     * @return bool
     */
    protected function usingDatabaseSessions()
    {
        return $this->manager->driver()->getHandler() instanceof DatabaseSessionHandler;
    }

    /**
     * Determine if the session is using cookie sessions.
     *
     * @return bool
     */
    protected function usingFileSessions()
    {
        return $this->manager->driver()->getHandler() instanceof FileSessionHandler;
    }
}
