<?php

namespace App\Http\Middleware;

use Closure;

/*
| 
| Para requisições vindas do front-end web app, 
| incluir as credencias da aplicação web.
|
*/

class PassportGrantTokensCredentials
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $origin = $request->header('Origin');
        if (appInDevelopment() || $origin && appDomain() == getDomain($origin)) {
            $add = [
                'client_id' => env('WEB_APP_CLIENT_ID'),
                'client_secret' => env('WEB_APP_CLIENT_SECRET')
            ];
            $request->headers->add($add);
            $request->request->add($add);
            $request->merge($add);
        }
        return $next($request);
    }
}
