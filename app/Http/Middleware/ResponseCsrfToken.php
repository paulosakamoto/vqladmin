<?php

namespace App\Http\Middleware;

use Closure;
use \Symfony\Component\HttpFoundation\Response;

class ResponseCsrfToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        
        if (is_object($response) && $response instanceof Response) {
            $token = csrf_token();
            if ($token) {
                $response->headers->set('X-Csrf-Token', $token);
            }
        }

        return $response;
    }
}
