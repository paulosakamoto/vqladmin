<?php

namespace App\Http\Middleware;

use Exception;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\OauthAccessToken;

class OauthLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $acao)
    {
        $response = $next($request);

        if (!$response instanceof Response) {
            return $response;
        }

        if ($request->method() === 'OPTIONS' || $response->isClientError()) {
            return $response;
        }

        if ($acao == 'login') {
            $this->login($response);
        }

        return $response;
    }

    /**
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    protected function login( Response $response)
    {
        $content = $response->getContent();
        $obj = json_decode($content);
        if (empty($obj->access_token)) {
            return;
        }

        try{
            $token = OauthAccessToken::fromJWT($obj->access_token);
            $userId = $token->user_id;

            if ($userId) {
                appAuditoria()->factory([
                    'descricao' => 'Efetuou o login',
                    'dados' => [
                        'tokenId' => $token->id
                    ],
                    'entidade' => 'sistema_usuario',
                    'entidade_id' => $userId,
                    'sistema_usuario_id' => $userId,
                ]);
            }

        } catch(Exception $e) {
        }
    }
}
