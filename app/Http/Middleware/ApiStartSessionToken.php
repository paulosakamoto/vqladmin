<?php

namespace App\Http\Middleware;

use Illuminate\Session\Middleware\StartSession;
use Exception;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Session\Session;

class ApiStartSessionToken extends StartSession
{
    /**
     * @var string|null
     */
    protected $sessionId;

    /**
     * @var \Laravel\Passport\Token
     */
    protected $authToken;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->getSessionId($request)) {
            return $next($request);
        }

        $this->sessionHandled = true;

        // If a session driver has been configured, we will need to start the session here
        // so that the data is ready for an application. Note that the Laravel sessions
        // do not make use of PHP "native" sessions in any way since they are crappy.
        if ($this->sessionConfigured()) {
            $request->setLaravelSession(
                $session = $this->startSession($request)
            );

            // $this->collectGarbage($session);
        }

        $response = $next($request);


        // Again, if the session has been configured we will need to close out the session
        // so that the attributes may be persisted to some storage medium. We will also
        // add the session identifier cookie to the application response headers now.
        if ($this->sessionConfigured()) {
            $this->storeCurrentUrl($request, $session);

            $this->persist($session);
        }

        return $response;
    }

    /**
     * Get the session implementation from the manager.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Session\Session
     */
    public function getSession(Request $request)
    {
        return tap($this->manager->driver(), function ($session) use ($request) {
            $session->setId($this->getSessionId($request));
        });
    }

    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    */
   
    /**
     * Get the session id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getSessionId(Request $request)
    {
        if (!$this->sessionId && $request->user() && $request->user()->token() && ($authorization = $request->header('Authorization')) && strpos($authorization, 'Bearer ') === 0) {
            
            $this->authToken = $request->user()->token();
            // O id deve ter 40 caracteres alfanuméricos
            // ver: \Illuminate\Session\Store@isValidId
            $origin = [
                'ip_address' => $request->ip(),
                'user_agent' => $request->userAgent(),
            ];

            $this->sessionId = 
                substr($request->user()->token()->id, 7, 8) . 
                md5($authorization . json_encode($origin));
        }
        
        return $this->sessionId;
    }

    /**
     * Add the session cookie to the application response.
     *
     * @param  \Illuminate\Contracts\Session\Session  $session
     * @return void
     */
    protected function persist(Session $session)
    {
        $this->manager->driver()->save();
    }
}
