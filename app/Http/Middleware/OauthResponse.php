<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class OauthResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
    
        if (!$response instanceof Response) {
            return $response;
        }

        if ($request->method() === 'OPTIONS') {
            return $response;
        }
        
        $content = $response->getContent();
        $status = $response->getStatusCode();
        
        if ($response->isClientError()) {
            $data = !is_array($content) ? json_decode($content, true) : [];
            $error = !empty($data['error']) ? $data['error'] : null; 
            $message = !empty($data['message']) ? $data['message'] : null; 
            $message = $error ? __('auth.'.$error) : $message;
            
            return response()->json(jsonReponseData(null, $message, false), $status);
        }

        if (empty($content) || $content == 'Refreshed.') {
            return response()->json(jsonReponseData(null, null, true), $status);
        }

        if ($content && substr($content, 0, 1) == '{') {
            $content = json_decode($content);
        }

        return response()->json(jsonReponseData($content, null, true), $status);
    }
}
