<?php 

namespace App\Relations;

use App\SistemaUsuario;

trait BelongsToSistemaUsuario
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sistemaUsuario()
    {
        return $this->belongsTo(SistemaUsuario::class);
    }
}