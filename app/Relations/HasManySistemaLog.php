<?php 

namespace App\Relations;

use App\SistemaLog;

trait HasManySistemaLog
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function sistemaLogs()
    {
        return $this->morphMany(SistemaLog::class, 'logable', 'entidade', 'entidade_id', 'id');
    }
}