<?php

namespace App\Services;

use App\SistemaLog;

class Auditoria
{
    /**
     * @param  $options array
     * @return \App\SistemaLog
     */
    public function factory(array $options = [])
    {
        return SistemaLog::factoryAuditoria($options);
    }
}
