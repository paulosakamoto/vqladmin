<?php

if (!function_exists('appEnv')) {
    /**
     * @return string
     */
    function appEnv() {
        return config('app.env') ?: 'local';
    }
}

if (!function_exists('appInProduction')) {
    /**
     * @return bool
     */
    function appInProduction() {
        return in_array(appEnv(), ['production', 'prod']);
    }
}

if (!function_exists('appInDevelopment')) {
    /**
     * @return bool
     */
    function appInDevelopment() {
        return !appInProduction();
    }
}

if (!function_exists('appUrl')) {
    /**
     * @return string
     */
    function appUrl() {
        return config('app.url');
    }
}

if (!function_exists('appDomain')) {
    /**
     * @return string
     */
    function appDomain() {
        return getDomain(appUrl());
    }
}

if (!function_exists('getDomain')) {
    /**
     * Retorna o domínio de uma URL.
     *
     * @param $url
     * @return string|null
     */
    function getDomain($url)
    {
        $url = str_replace(" ","",trim($url));

        $parse = parse_url($url);
        if (isset($parse['host'])) {
            return $parse['host'];
        }

        if (isset($parse['path'])) {
            $tmp = explode('/', $parse['path']);
            return ! empty($tmp[0]) ? $tmp[0] : null;
        }

        return null;
    }
}

if (!function_exists('jsonReponseData')) {
    /**
     * @return array
     */
    function jsonReponseData($dados = null, $mensagem = null, $sucesso = true)
    {
        return [
            'sucesso' => $sucesso,
            'mensagem' => $mensagem,
            'dados' => $dados
        ];
    }
}

if (!function_exists('morphMap')) {
    function morphMap()
    {
        return [
            'oauth_access_tokens'   => 'App\OauthAccessToken',
            'sistema_perfil'        => 'App\SistemaPerfil',
            'sistema_permissao'     => 'App\SistemaPermissao',
            'sistema_usuario'       => 'App\SistemaUsuario',
        ];
    }
}

if (!function_exists('appAuditoria')) {
    function appAuditoria()
    {
        return resolve(\App\Services\Auditoria::class);
    }
}

if (!function_exists('appSensitiveData')) {
    function appSensitiveData()
    {
        return resolve(\App\Foundation\Utils\SensitiveData::class);
    }
}

