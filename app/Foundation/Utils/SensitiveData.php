<?php

namespace App\Foundation\Utils;

class SensitiveData
{
    /**
     * @var array
     */
    protected $sensitiveKeys = [
        'password',
        'password1',
        'password2',
        'pass',
        'pwd',
        'senha',
        'senha1',
        'senha2',
        'nova_senha',
        'novasenha',
        'novaSenha',
        'senha_antiga',
        'senhaAntiga',
        'senhaantiga',
        'new_password',
        'newpassword',
        'newPassword',
        'old_password',
        'oldPassword',
        'oldpassword',
        'repeat_password',
        'repeatpassword',
        'repeatPassword',
        'confirm_password',
        'confirmpassword',
        'confirmPassword',
        'confirm_pass',
        'confirm_pws',
        'confirmar_senha',
        'confirmarsenha',
        'confirmarSenha',
        '_token',
        'token',
        'csrftoken',
        'csrf',
        'cc_number',
        'ccnumber',
        'cc_exp_month',
        'cc_exp_year',
        'cc_cvc',
        'cccvc',
    ];

    /**
     * @param array|object $data
     * @param string $mask
     * @return array
     */
    public function mask($data, $mask = '***')
    {
        if (!is_array($data) && !is_object($data)) {
            return $data;
        }
        $masked = [];
        foreach ($data as $key => $value){
            if (is_array($value) || is_object($value)) {
                $masked[$key] = $this->mask($value, $mask);
                continue;
            }
            if (is_string($key) && in_array($key, $this->sensitiveKeys)) {
                $value = $mask;
            }
            $masked[$key] = $value;
        }
        return $masked;
    }
}
