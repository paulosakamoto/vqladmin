<?php

namespace App\Foundation\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Relations\HasManySistemaLog;

abstract class BaseModel extends Model
{
    use HasManySistemaLog;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'int';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable()
    {
        if (empty($this->table)) {
            return str_replace(
                '\\', '', Str::snake(class_basename($this))
            );
        }

        return $this->table;
    }
}
