<?php

namespace App\Foundation\Model\Enum;

trait EnumAtivo
{
    /**
     * @var array
     */
    protected static $enumAtivo = ['Sim', 'Não'];

    /**
     * @var array
     */
    public static function enumAtivo()
    {
        return static::$enumAtivo;
    }

    /**
     * @return string
     */
    public function getAtivoSim()
    {
        return 'Sim';
    }

    /**
     * @return string
     */
    public function getAtivoNao()
    {
        return 'Não';
    }
}
