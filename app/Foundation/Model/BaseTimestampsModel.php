<?php

namespace App\Foundation\Model;

abstract class BaseTimestampsModel extends BaseModel
{
    /**
     * @var string
     */
    const CREATED_AT = 'criado_em';

    /**
     * @var string
     */
    const UPDATED_AT = 'atualizado_em';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
