<?php

namespace App\Foundation\Model;

use Illuminate\Database\Eloquent\Model;

abstract class BaseTimestampsSoftDeletesModel extends BaseTimestampsModel
{
    use SoftDeletes;
    
    /**
     * @var string
     */
    const DELETED_AT = 'excluido_em';

    /**
     * @var string
     */
    const ATIVO_SIM = 'Sim';

    /**
     * @var string
     */
    const ATIVO_NAO = 'Não';
}
