<?php

namespace App;

use App\Foundation\Model\BaseModel;
use App\Relations\BelongsToSistemaUsuario;
use App\Enum\EnumSistemaLogTipo;
use Auth;

class SistemaLog extends BaseModel
{
    use BelongsToSistemaUsuario, EnumSistemaLogTipo;

    /**
     * @var string
     */
    const TIPO_AUDITORIA = 'Auditoria';

    /**
     * @var string
     */
    const TIPO_LOG = 'Log';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'dados' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function logable()
    {
        return $this->morphTo();
    }

    /**
     * @param  $options array
     * @return \App\SistemaLog
     */
    public static function factory(array $options = [])
    {
        if (!isset($options['sistema_usuario_id'])) {
            $options['sistema_usuario_id'] = Auth::id() ?: null;
        }

        if (empty($options['tipo'])) {
            $options['tipo'] = static::TIPO_LOG;
        }

        if (empty($options['data'])) {
            $options['data'] = date('Y-m-d');
        }

        if (empty($options['hora'])) {
            $options['hora'] = date('H:i:s');
        }

        if (empty($options['ip'])) {
            $options['ip'] = request()->ip() ?: '0.0.0.0';
        }

        if (empty($options['descricao'])) {
            $options['descricao'] = '';
        }

        if (empty($options['dados'])) {
            $options['dados'] = [];
        } else {
            $options['dados'] = appSensitiveData()->mask($options['dados']);
        }

        if (empty($options['criado_em'])) {
            $options['criado_em'] = date('Y-m-d H:i:s');
        }

        $model = static::forceCreate($options);

        return $model;
    }

    /**
     * @param  $options array
     * @return \App\SistemaLog
     */
    public static function factoryAuditoria(array $options = [])
    {
        $options['tipo'] = static::TIPO_AUDITORIA;

        return static::factory($options);
    }

    /**
     * @param  $options array
     * @return \App\SistemaLog
     */
    public static function factoryLog(array $options = [])
    {
        $options['tipo'] = static::TIPO_LOG;

        return static::factory($options);
    }
}
