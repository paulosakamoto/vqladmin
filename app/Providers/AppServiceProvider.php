<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Foundation\Utils\SensitiveData;
use App\Services\Auditoria;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap(morphMap());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Auditoria::class, function(){
            return new Auditoria;
        });

        $this->app->singleton(SensitiveData::class, function(){
            return new SensitiveData;
        });
    }
}
