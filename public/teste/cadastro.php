<?php
/*
CREATE TABLE `teste`.`clientes`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `usuario` VARCHAR(30) NOT NULL,
  `senha` VARCHAR(255) NOT NULL,
  `cpf` VARCHAR(14) NOT NULL,
  `telefone` VARCHAR(15) NOT NULL,
  `ativo` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `clientes_email_unique` (`email`),
  UNIQUE INDEX `clientes_usuario_unique` (`usuario`),
  INDEX `clientes_ativo_email_index` (`email`, `ativo`),
  INDEX `clientes_ativo_usuario_index` (`usuario`, `ativo`)
);
*/
function conecta(){
    $mysqli = new mysqli('localhost', 'usuario-do-banco', 'senha', 'nome-do-banco');
    if ($mysqli->connect_errno) {
        throw new Exception(sprintf('Não foi possível contectar ao banco de dados. Motivo:', $mysqli->connect_error));
    }
    return $mysqli;
}

function normalizaDados(array $post){
    $nome       = !empty($post['nome']) ? trim($post['nome']) : null;
    $email      = !empty($post['email']) ? trim($post['email']) : null;
    $usuario    = !empty($post['usuario']) ? trim($post['usuario']) : null;
    $cpf        = !empty($post['cpf']) ? trim($post['cpf']) : null;
    $telefone   = !empty($post['telefone']) ? trim($post['telefone']) : null;
    $senha      = !empty($post['senha']) ? trim($post['senha']) : null;
    $confirmar  = !empty($post['confirmar-senha']) ? trim($post['confirmar-senha']) : null;

    return [
        'nome'      => $nome,
        'email'     => $email,
        'usuario'   => $usuario,
        'cpf'       => $cpf,
        'telefone'  => $telefone,
        'senha'     => $senha,
        'confirmar' => $confirmar,
    ];
}

function validaCamposObrigatorios(array $dados){
    //todos os campos sao obrigatorios
    foreach ($dados as $key => $value) {
        if (empty($value)) {
            throw new Exception(sprintf('O campo "%s" é obrigatório.', $key));
        }
    }
}

function validaSenha($senha, $confirmar){
    if (strlen($senha) < 6) {
        throw new Exception('A senha deve ter ao menos 6 caracteres.');
    }
    if ($senha !== $confirmar) {
        throw new Exception('As senhas não conferem.');
    }
}

function validaEmail($dbcon, $email){
    $stmt = $dbcon->prepare('SELECT `id` FROM `clientes` WHERE `email` = ? LIMIT 1;');
    if (!$stmt) {
        throw new Exception('Erro ao preparar a consulta de validação de e-mail.');
    }
    $stmt->bind_param('s', $email);
    if (!$stmt->execute()) {
        $stmt->close();
        throw new Exception('Erro ao executar a consulta de validação de e-mail.');
    }
    $stmt->bind_result($id);
    $stmt->fetch();
    $stmt->close();
    if ($id) {
        throw new Exception('E-mail já utilizado.');
    }
}

function validaUsuario($dbcon, $email){
    $stmt = $dbcon->prepare('SELECT `id` FROM `clientes` WHERE `usuario` = ? LIMIT 1;');
    if (!$stmt) {
        throw new Exception('Erro ao preparar a consulta de validação de usuário.');
    }
    $stmt->bind_param('s', $email);
    if (!$stmt->execute()) {
        $stmt->close();
        throw new Exception('Erro ao executar a consulta de validação de usuário.');
    }
    $stmt->bind_result($id);
    $stmt->fetch();
    $stmt->close();
    if ($id) {
        throw new Exception('Usuário já utilizado.');
    }
}

function validaDados($dbcon, array $dados){
    validaCamposObrigatorios($dados);
    validaSenha($dados['senha'], $dados['confirmar']);
    validaEmail($dbcon, $dados['email']);
    validaUsuario($dbcon, $dados['usuario']);
}

function insereDados($dbcon, array $dados){
    $stmt = $dbcon->prepare('INSERT INTO `clientes`(`nome`, `email`, `usuario`, `cpf`, `telefone`, `senha`, `ativo`) VALUES(?, ?, ?, ?, ?, ?, 1);');
    if (!$stmt) {
        throw new Exception('Erro ao preparar a consulta de criação do cliente.');
    }
    $stmt->bind_param(
        'ssssss', 
        $dados['nome'], 
        $dados['email'], 
        $dados['usuario'], 
        $dados['cpf'], 
        $dados['telefone'], 
        md5($dados['senha']) 
    );
    if (!$stmt->execute()) {
        $error = $stmt->error;
        $stmt->close();
        throw new Exception('Erro ao executar a consulta de criação de cliente: ' . $error);
    }
    $stmt->close();
    return $dbcon->insert_id;
}

$mensagem = '';

$dados = normalizaDados($_POST);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    try{
        $dbcon = conecta();

        validaDados($dbcon, $dados);

        insereDados($dbcon, $dados);

        //para redirecionar para o login.php
        //tirar o comentário das linhas abaixo.
        //header('Location: /login.php');
        //exit;
        
        $mensagem = 'Usuário inserido com sucesso.';

        $dados = normalizaDados([]);
    } catch (Exception $e) {
        $mensagem = $e->getMessage();
    }
}
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Cadastro</title>
    </head>
    <body>
        <form method="POST" id="formulario" onsubmit="validaFormulario(event)">
            <div id="mensagem"><?php echo $mensagem; ?></div>

            <div>
                <label for="nome">Nome: *</label>
                <input type="text" required minlength="6" id="nome" name="nome" placeholder="Nome" value="<?php echo $dados['nome']; ?>">
            </div>

            <div>
                <label for="email">E-mail: *</label>
                <input type="email" required id="email" name="email" placeholder="E-mail" value="<?php echo $dados['email']; ?>">
            </div>
            
            <div>
                <label for="usuario">Usuário: *</label>
                <input type="text" required minlength="4" id="usuario" name="usuario" placeholder="Usuário" value="<?php echo $dados['usuario']; ?>">
            </div>

            <div>
                <label for="senha">Senha: *</label>
                <input type="password" required minlength="6" id="senha" name="senha" placeholder="Insira senha" onkeyup="comparaSenhas()">
            </div>

            <div>
                <label for="confirmar-senha">Confirmar senha: *</label>
                <input type="password" required minlength="6" id="confirmar-senha" name="confirmar-senha" placeholder="Confirme a senha " onkeyup="comparaSenhas()">
            </div>
            
            <div>
                <label for="cpf">CPF: *</label>
                <input type="text" required id="cpf" name="cpf" placeholder="CPF" value="<?php echo $dados['cpf']; ?>">
            </div>

            <div>
                <label for="telefone">Telefone: *</label>
                <input type="text" required id="telefone" name="telefone" placeholder="Telefone" value="<?php echo $dados['telefone']; ?>">
            </div>

            <div>
                <input type="submit" value="Salvar">
            </div>
        </form>

        <script>
            var $senha = document.getElementById('senha');
            var $confirmarSenha = document.getElementById('confirmar-senha');
            var $mensagem = document.getElementById('mensagem');

            function comparaSenhas(){
                if ($senha.value && $confirmarSenha.value && $senha.value == $confirmarSenha.value) {
                    $mensagem.innerText = '';
                    return true;
                } 
                if ($senha.value && $confirmarSenha.value) {
                    $mensagem.innerText = 'As senhas não conferem.';
                }
                return false;
            }

            function validaFormulario(event) {
                console.log('validando o formulário...');
                if (!comparaSenhas()) {
                    console.log('formulário inválido! submit impedido.');
                    event.preventDefault();
                }
                console.log('formulário valido!');
            }
        </script>
    </body>
</html>