import { SessionStorage } from 'quasar'

export default {
    SET_USUARIO (state, usuario) {
        state.id = usuario.id ? usuario.id : null
        state.nome = usuario.nome ? usuario.nome : null
        state.email = usuario.email ? usuario.email : null
        
        let permissoes = usuario.permissoes ? usuario.permissoes : null
        let t = typeof permissoes
        if (t != 'object') {
            permissoes = t == 'string' ? [permissoes] : []
        }
        let k, permissoesTratadas = []
        for(k in permissoes) {
            if (permissoes[k]) {
                permissoesTratadas.push(permissoes[k].toLowerCase())
            }
        }
        state.permissoes = permissoesTratadas

        SessionStorage.set('usuario', state)
    }
}
