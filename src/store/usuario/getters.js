
export default {
    getId: state => state.id,
    getNome: state => state.nome,
    getEmail: state => state.email,
    getPermissoes: state => state.permissoes,
    getUsuario: state => state
}
