import { SessionStorage } from 'quasar'

let usuario = SessionStorage.has('usuario') ? SessionStorage.get.item('usuario') : {};
usuario = typeof usuario == 'object' ? usuario : {}

export default {
    id: usuario.id ? usuario.id : null,
    nome: usuario.nome ? usuario.nome : null,
    email: usuario.email ? usuario.email : null,
    permissoes: usuario.permissoes ? usuario.permissoes : []
}
