import Vue from 'vue'
import Vuex from 'vuex'

import app from './app'
import layout from './layout'
import usuario from './usuario'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        app,
        layout,
        usuario
    }
})

export default store
