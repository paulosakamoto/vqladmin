
export default {
    setLeftDrawerOpen ({commit}, leftDrawerOpen) {
        commit('SET_LEFT_DRAWNER_OPEN', leftDrawerOpen)
    },
    toggleLeftDrawerOpen ({commit}) {
        commit('TOGGLE_LEFT_DRAWNER_OPEN')
    },
    setBannerTitle ({commit}, bannerTitle) {
        commit('SET_BANNER_TITLE', bannerTitle)
    },
    setBannerShow ({commit}, bannerShow) {
        commit('SET_BANNER_SHOW', bannerShow)
    }
}
