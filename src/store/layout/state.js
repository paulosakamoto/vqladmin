import { LocalStorage, Platform } from 'quasar'

export default {
    leftDrawerOpen: 
        LocalStorage.has('layout.leftDrawerOpen') ? 
        !!LocalStorage.get.item('layout.leftDrawerOpen') : 
        Platform.is.desktop, 
    bannerTitle: '',
    bannerShow: true
}
