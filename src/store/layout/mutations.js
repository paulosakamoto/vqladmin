import { LocalStorage } from 'quasar'

export default {
    SET_LEFT_DRAWNER_OPEN (state, leftDrawerOpen) {
        state.leftDrawerOpen = leftDrawerOpen
        LocalStorage.set('layout.leftDrawerOpen', state.leftDrawerOpen)
    },
    TOGGLE_LEFT_DRAWNER_OPEN (state) {
        state.leftDrawerOpen = !state.leftDrawerOpen
    },
    SET_BANNER_TITLE (state, bannerTitle) {
        state.bannerTitle = bannerTitle
    },
    SET_BANNER_SHOW (state, bannerShow) {
        state.bannerShow = !!bannerShow
    }
}
