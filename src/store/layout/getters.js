
export default {
    getLeftDrawerOpen: state => state.leftDrawnerOpen,
    getBannerTitle: state => state.bannerTitle,
    getBannerShow: state => state.bannerShow
}
