import { SessionStorage } from 'quasar'

let authToken = SessionStorage.has('authToken') ? SessionStorage.get.item('authToken') : {};
authToken = typeof authToken == 'object' ? authToken : {}

export default process.env.DEV ? {
    name: 'VQL Admin',
    shortDescription: 'Vue + Quasar + Laravel',
    version: '0.1.2',
    env: 'development',
    dev: true,
    prod: false,
    //para rodar em dominios diferentes. Para o mesmo dominio, deixar em branco
    api: 'http://dev.vqladmin.pssolucoes.net.br',
    auth: {
        autocheck: true,
        interval: 300000,
        autocheckUrl: '/api/v1/me',
        refreshUrl: '/api/v1/oauth/token',
        token: {
            id: authToken.id ? authToken.id : null,
            tokenType: authToken.tokenType ? authToken.tokenType : 'Bearer',
            expiresIn: authToken.expiresIn ? authToken.expiresIn : null,
            expiresAt: authToken.expiresAt ? authToken.expiresAt : null,
            accessToken: authToken.accessToken ? authToken.accessToken : null,
            refreshToken: authToken.refreshToken ? authToken.refreshToken : null
        }
    },
    route: {
        notFound: {name: 'notFound'},
        error: {name: 'erro'},
        login: {name: 'auth.login'},
        logout: {name: 'auth.logout'}
    },
    csrfToken: {
        url: '/api/v1/csrfToken',
        token: null
    }
} : {
    name: 'VQL Admin',
    shortDescription: 'Vue + Quasar + Laravel',
    version: '0.1.1',
    env: 'production',
    dev: false,
    prod: true,
    //para rodar em dominios diferentes. Para o mesmo dominio, deixar em branco
    api: '',
    auth: {
        autocheck: true,
        interval: 300000,
        autocheckUrl: '/api/v1/me',
        refreshUrl: '/api/v1/oauth/token',
        token: {
            id: authToken.id ? authToken.id : null,
            tokenType: authToken.tokenType ? authToken.tokenType : 'Bearer',
            expiresIn: authToken.expiresIn ? authToken.expiresIn : null,
            expiresAt: authToken.expiresAt ? authToken.expiresAt : null,
            accessToken: authToken.accessToken ? authToken.accessToken : null,
            refreshToken: authToken.refreshToken ? authToken.refreshToken : null
        }
    },
    route: {
        notFound: {name: 'notFound'},
        error: {name: 'erro'},
        login: {name: 'auth.login'},
        logout: {name: 'auth.logout'}
    },
    csrfToken: {
        url: '/api/v1/csrfToken',
        token: null
    }
}
