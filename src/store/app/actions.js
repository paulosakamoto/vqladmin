
export default {
    setCsrfToken ({commit}, csrfToken) {
        commit('SET_CSRF_TOKEN', csrfToken)
    },
    setAuthToken ({commit}, authToken) {
        commit('SET_AUTH_TOKEN', authToken)
    }
}
