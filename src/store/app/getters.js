export default {
    getName: state => state.name,
    getVersion: state => state.version,
    getShortDescription: state => state.shortDescription,
    getCsrfToken: state => state.csrfToken,
    getApi: state => state.api
}
