import { SessionStorage } from 'quasar'

export default {
    SET_CSRF_TOKEN (state, csrfToken) {
        state.csrfToken.token = csrfToken
    },
    SET_AUTH_TOKEN (state, authToken) {
        state.auth.token.tokenType = authToken.token_type
        state.auth.token.expiresIn = authToken.expires_in
        state.auth.token.expiresAt = (Math.floor(Date.now()/1000)) + authToken.expires_in
        state.auth.token.accessToken = authToken.access_token
        state.auth.token.refreshToken = authToken.refresh_token
        state.auth.token.id = null

        if (authToken.access_token && authToken.access_token.indexOf('.') != -1) {
            //decodifica o header do jwt
            try{
                let partials = authToken.access_token.split('.')
                let headerJSON = partials[0] ? atob(partials[0]) : null
                let payloadJSON = partials[1] ? atob(partials[1]) : null
                let header = headerJSON ? JSON.parse(headerJSON) : {}
                let payload = payloadJSON ? JSON.parse(payloadJSON) : {}
                /*
                {
                  "typ": "JWT",
                  "alg": "RS256",
                  "jti": "xxx"
                }
                {
                  "aud": "",
                  "jti": "",
                  "iat": ,
                  "nbf": ,
                  "exp": ,
                  "sub": "",
                  "scopes": []
                }
                 */
                if (header && header.typ == 'JWT' && header.jti) {
                    state.auth.token.id = header.jti
                }
                if (payload && payload.exp) {
                    state.auth.token.expiresAt = parseInt(payload.exp)
                }
            } catch(e){}
        }

        SessionStorage.set('authToken', state.auth.token)
    }
}
