import LayoutDefault from '../layouts/default'

/*
|--------------------------------------------------------------------------
| Importante
|--------------------------------------------------------------------------
|
| é obrigatório configurar em cada rota um "meta.middleware", 
| mesmo que seja vazio. Exemplo: 
| { path: '/contato', component: () => import('pages/contato'), name: 'contato', meta: {middleware: []}  }
| 
| Middlewares disponíveis:
| 
| "auth" - o usuário deve estar logado 
| "auth:administrador" - o usuário deve ter um perfil de administrador (perfil.administrador)
| "auth:admin" - alias para "auth:administrador"
| "auth:permissao,nome" - o usuário deve ter acesso a permissão com o nome "nome"
| "auth:perfil,nome" - o usuário deve ter o perfil com nome "nome"
| 
*/

export default [
    {
        path: '/',
        component: LayoutDefault,
        children: [
            { path: '/', redirect: '/dashboard', name: 'index', meta: {middleware: ['auth']} },
            { path: '/home', redirect: { name: 'dashboard' }, name: 'home', meta: {middleware: ['auth']}  },
            { path: '/dashboard', component: () => import('pages/dashboard'), name: 'dashboard', meta: {middleware: ['auth']}  },
            { path: '/sobre', component: () => import('pages/sobre'), name: 'sobre', meta: {middleware: ['auth:admin']}  },
            { path: '/sistema', component: () => import('pages/sistema'), name: 'sistema', meta: {middleware: ['auth:perfil,administrador']}  },
            { path: '/contato', component: () => import('pages/contato'), name: 'contato', meta: {middleware: []}  }
        ]
    },

    {
        path: '/auth',
        component: LayoutDefault,
        children: [
            { path: '/perfil', component: () => import('pages/auth/perfil'), name: 'auth.perfil', meta: {middleware: ['auth']}  },
            { path: '/logout', component: () => import('pages/auth/logout'), name: 'auth.logout', meta: {middleware: []} },
            { path: '/login', component: () => import('pages/auth/login'), name: 'auth.login', meta: {middleware: []} }
        ]
    },
    { 
        path: '/erro',
        name: 'erro',
        component: () => import('pages/erro'),
        meta: {middleware: []}
    },
    { 
        path: '/404',
        name: 'notFound',
        component: () => import('pages/404'),
        meta: {middleware: []}
    },
    { // Always leave this as last one
        path: '*',
        component: () => import('pages/404'),
        meta: {middleware: []}
    }
]
