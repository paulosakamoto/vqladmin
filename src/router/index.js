import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import AuthService from '../services/auth.js'

Vue.use(VueRouter)

const Router = new VueRouter({
    /*
    * NOTE! Change Vue Router mode from quasar.conf.js -> build -> vueRouterMode
    *
    * If you decide to go with "history" mode, please also set "build.publicPath"
    * to something other than an empty string.
    * Example: '/' instead of ''
    */

    // Leave as is and change from quasar.conf.js instead!
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
    scrollBehavior: () => ({ y: 0 }),
    routes
})

/*  
Router.beforeEach((to, from, next) => {
    // antes de cada rota
    console.log('router.index.js.beforeEach')
    next()
})

Router.afterEach((to, from) => {
    // depois de cadas rota
})
*/

export default Router
