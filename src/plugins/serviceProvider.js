import Auth from '../services/auth.js'
import {httpService} from '../services/http.js'
import Middleware from '../services/middleware.js'
import Notify from '../services/notify.js'
import Vuelidate from 'vuelidate'
import '../lib/mixins.js'

// para ativar o plugin, inserir "serviceProvider" no parâmetro 
// "plugins" do arquivo quasar.conf.js

export default ({ app, router, store, Vue }) => {
    Vue.use(Auth, { app, router, store, Vue })
    Vue.use(httpService, { app, router, store, Vue })
    Vue.use(Middleware, { app, router, store, Vue })
    Vue.use(Notify, { app, router, store, Vue })
    Vue.use(Vuelidate, { app, router, store, Vue })
}
