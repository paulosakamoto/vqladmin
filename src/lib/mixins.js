import Vue from 'vue'
import {openURL} from 'quasar'
import { httpHelpers } from '../services/http.js'

Vue.mixin({
    methods: {
        hello: function () {
            console.log('hello from mixin')
        },
        openURL (url) {
            openURL(url)
        },
        routeTo (url) {
            this.$router.push(url)
        },
        getResponseData (response) {
            return httpHelpers.getResponseData(response)
        } 
    }
})
