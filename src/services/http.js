import axios from 'axios'
import { Service } from 'axios-middleware'

let Vue
let $router
let $store
let app

let defaultTokenUrl = '/api/v1/csrfToken'
let tokenUrl = defaultTokenUrl
let defaultRefreshUrl = '/api/v1/oauth/token'
let refreshUrl = refreshUrl
let checkExpiresAtInterval
//verificar a necessidade de atualizar o token (refresh_token)
//a cada 10 minutos (verificação somente no javascript com "setinterval")
let checkExpiresAtIntervalSeconds = 600000

let $token = {
    updateCsrfToken (t) {
        $store.dispatch('app/setCsrfToken', t)
    },
    async refreshAccessToken () {
        axios.post(refreshUrl, {
            grant_type: 'refresh_token',
            refresh_token: $store.state.app.auth.token.refreshToken
        })
        .then((res) => {
            let data = httpHelpers.getResponseData(res)
            console.log('http.js => refreshAccessToken.data: ', data)
            if (data.dados.access_token) {
                this.updateAuthToken(data.dados)
            }
        })
    },
    updateAuthToken (t) {
        $store.dispatch('app/setAuthToken', t)
    },
    getFromResponse (response) {
        return response.headers && response.headers['x-csrf-token'] ? response.headers['x-csrf-token'] : null
    },
    checkFromResponse (response) {
        let t = this.getFromResponse(response)
        if (t) {
            this.updateCsrfToken(t)
        }
    },
    async start (cb) {
        try{
            axios.get(tokenUrl)
            .then((res) => {
                if (res.data && res.data.sucesso && res.data.dados && res.data.dados.token) {
                    this.updateCsrfToken(res.data.dados.token)
                }

            })
            .catch(() => {

            })
            .then(() => {
                if (cb) {
                    cb(this.csrfToken())
                }
            })
        } catch (e){}

        if (!checkExpiresAtInterval) {
            checkExpiresAtInterval = setInterval(() => {
                this.checkExpiresAt()
            }, checkExpiresAtIntervalSeconds)
        }
    },
    checkExpiresAt () {
        let seconds, now
        if ($store.state.app.auth.token.id && $store.state.app.auth.token.refreshToken && $store.state.app.auth.token.expiresAt) {
            now = Math.floor(Date.now()/1000)
            seconds = $store.state.app.auth.token.expiresAt - now
            //considerar uma diferença de timezone? 
            //consultar essa informação no servidor?
            if (seconds < 14400) {
                this.refreshAccessToken()
            }
        }
    },
    csrfToken () {
        return $store.state.app.csrfToken.token
    },
    authToken () {
        return $store.state.app.auth.token
    },
    accessToken () {
        return this.authToken().accessToken
    },
    refreshToken () {
        return this.authToken().refreshToken
    }
}

const service = new Service(axios)

service.register({
    onRequest (config) {
        config.headers['X-Requested-With'] = 'XMLHttpRequest'
        config.withCredentials = true
        config.responseType = 'json'
        if ($token.csrfToken()) {
            config.headers['X-Csrf-Token'] = $token.csrfToken()
        }
        if ($token.accessToken()) {
            let type = $token.authToken().tokenType ? $token.authToken().tokenType : 'Bearer'
            config.headers['Authorization'] =  type + ' ' + $token.accessToken()
        }
        if (!config.baseURL && config.url.indexOf('http') === -1) {
            if (config.url.indexOf('/') === -1) {
                config.url = '/' + config.url
            }
            config.url = $store.state.app.api + config.url
        }
        return config
    },
    onSync (promise) {
        return promise
    },
    onResponse (response) {
        $token.checkFromResponse(response)
        return response
    },
    onRequestError (error) {
        return error
    },
    onResponseError (error) {
        return error
    }
})

const $http = axios

const httpService = {
    install (Vue, options) {
        if (!options.Vue) {
            throw 'O parâmetro "Vue" não foi passado corretamente'
        }

        if (!options.store) {
            throw 'O parâmetro "store" não foi passado corretamente'
        }

        if (!options.app) {
            throw 'O parâmetro "app" não foi passado corretamente'
        }

        if (!options.router) {
            throw 'O parâmetro "Router" não foi passado corretamente'
        }

        Vue = options.Vue
        $router = options.router
        $store = options.store
        app = options.app

        if ($store.state.app.csrfToken.url) {
            tokenUrl = $store.state.app.csrfToken.url
        }

        if ($store.state.app.auth.refreshUrl) {
            refreshUrl = $store.state.app.auth.refreshUrl
        }

        $token.start()

        Object.defineProperties(Vue.prototype, {
            $http: {
                get: function () {
                    return axios
                }
            }
        })
    }
}

const httpHelpers = {
    parseResponseDados(dados){
        let t = typeof dados
        if (!dados) {
            return dados
        }
        
        if (t == 'undefined' || (t == 'string' && dados == 'undefined')) {
            return null
        }

        if (t == 'object') {
            return dados
        }

        if (t == 'string') {
            if (dados.indexOf('{') === 0) {
                try{
                    return JSON.parse(dados)
                }catch (e){
                    return {}
                }
            } 
            if (dados.indexOf('[') === 0) {
                try{
                    return JSON.parse(dados)
                }catch (e){
                    return []
                }
            }
            return dados
        }

        return dados
    },
    getResponseData (response) {
        let data = {
            sucesso: null,
            mensagem: null,
            dados: null
        }
        if (!typeof response == 'object') {
            return data
        }
        if (typeof response['data'] == 'object') {
            data.sucesso = !!response.data.sucesso
            data.mensagem = response.data.mensagem ? response.data.mensagem : null
            data.dados = response.data.dados ? this.parseResponseDados(response.data.dados) : null
        } else if (typeof response['response'] == 'object' && typeof response.response['data'] == 'object') {
            data.sucesso = !!response.response.data.sucesso
            data.mensagem = response.response.data.mensagem ? response.response.data.mensagem : null
            data.dados = response.response.data.dados ? this.parseResponseDados(response.response.data.dados) : null
        }
        return data
    }
}

export { httpService, $http, $token, httpHelpers }

export default $http
