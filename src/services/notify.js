import axios from 'axios'
import { Service } from 'axios-middleware'
import { Notify } from 'quasar'
import { httpHelpers } from './http.js'

let Vue
let $router
let $store
let app

const service = new Service(axios)

const notify = function (success, message) {
    Notify.create({
        message: message,
        timeout: 5000, // in milliseconds; 0 means no timeout
        // Available values: 'positive', 'negative', 'warning', 'info'
        type: success ? 'positive' : 'negative',
        color: success ? 'positive' : 'negative',
        textColor: 'black', // if default 'white' doesn't fits
        icon: 'fa fa-check',
        // avatar: 'statics/boy-avatar.png',
        detail: '',
        position: 'top-right',
        actions: [
            {
                label: 'x'
            }
        ]
    })
}

service.register({
    onResponse (response) {
        let data = httpHelpers.getResponseData(response)
        let routeName = $router.currentRoute && $router.currentRoute.name ? $router.currentRoute.name : null
        if (routeName && routeName.indexOf('logout') != -1) {
            return response
        }

        if (data.mensagem) {
            notify(!!data.sucesso, data.mensagem)
        } else if (data.sucesso === null) {
            let str = response.toString();
            if (str.indexOf('Error') !== -1) {
                if (str.indexOf('Network') !== -1) {
                    notify(false, 'Erro de conexão com o servidor')    
                } else if (str.indexOf('status code 50') !== -1) {
                    notify(false, 'O servidor retornou um erro inesperado')
                }
            }
        }
        return response
    },
    onRequestError (error) {
        return error
    },
    onResponseError (error) {
        console.log('notify.js => onResponse.error:, ', error)
        console.log('notify.js => onResponse.route:, ', $router.route)
        return error
    }
})

export default {
    install (Vue, options) {
        if (!options.Vue) {
            throw 'O parâmetro "Vue" não foi passado corretamente'
        }

        if (!options.store) {
            throw 'O parâmetro "store" não foi passado corretamente'
        }

        if (!options.app) {
            throw 'O parâmetro "app" não foi passado corretamente'
        }

        if (!options.router) {
            throw 'O parâmetro "Router" não foi passado corretamente'
        }

        Vue = options.Vue
        $router = options.router
        $store = options.store
        app = options.app
    }
}
