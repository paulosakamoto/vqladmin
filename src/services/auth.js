import { CreateMiddleware } from './middleware.js'
import { $http, $token } from './http.js'

let Vue
let $router
let $store
let app

// 5 minutos
let defaultAutoCheckInterval = 300000
let defaultUseAutocheck = true
let defaultAutoCheckUrl = '/api/v1/me'
let defaultRouteError = {name: 'erro'}
let defaultRouteLogin = {name: 'auth.login'}
let defaultRouteLogout = {name: 'auth.logout'}

let autoCheckUrl = defaultAutoCheckUrl
let useAutoCheck = defaultUseAutocheck
let autoCheckInterval = defaultAutoCheckInterval
let routeError = defaultRouteError
let routeLogin = defaultRouteLogin
let routeLogout = defaultRouteLogout

let auth = {
    async apiCheck (cb) {
        $http.get(autoCheckUrl)
            .then((res) => {
                if (res.data && res.data.sucesso && res.data.dados) {
                    $store.dispatch('usuario/setUsuario', res.data.dados)
                    if (cb) {
                        cb(true, res.data.dados, res)
                    }
                } else {
                    console.error('auth.js => Usuário não autenticado no servidor: ', res.data)
                    $router.push(routeLogout)
                    if (cb) {
                        cb(false, {}, err)
                    }
                }
            }).catch((err) => {
                console.error('auth.js => Erro no servidor', err)
                $router.push(routeLogout)
                if (cb) {
                    cb(false, {}, err)
                }
            })
    },
    check () {
        return !!(this.id())
    },
    usuario (key, def) {
        let usuario = $store ? $store.state.usuario : {}
        usuario = typeof usuario == 'object' ? usuario : {}
        if (key) {
            def = def ? def : null
            return typeof usuario[key] != 'undefined' ? usuario[key] : def
        }
        return usuario
    },
    id () {
        return this.usuario('id')
    },
    nome () {
        return this.usuario('nome')
    },
    email () {
        return this.usuario('email')
    },
    permissoes () {
        let permissoes = this.usuario('permissoes', [])
        return typeof permissoes == 'object' ? permissoes : []
    },
    permissao (permissao) {
        if (!this.check() || !this.permissoes() || !permissao) {
            return false
        }
        return this.permissoes().indexOf(permissao.toLowerCase()) !== -1
    },
    perfil (perfil) {
        if (perfil.indexOf('perfil.') === -1) {
            perfil = 'perfil.' + perfil
        }
        return this.permissao(perfil)
    },
    perfilAdministrador () {
        return this.perfil('administrador')
    },
    login (usuario, authToken) {
        $store.dispatch('usuario/setUsuario', usuario)
        $token.updateAuthToken(authToken)
    },
    logout () {
        $store.dispatch('usuario/setUsuario', {})
        $token.updateAuthToken({})
    }
}

let authMiddleware = function (options, route) {
    if (!auth.check()) {
        throw {route: routeLogin.name}
    }
    let key = options.key
    if (!key) {
        return
    }
    key = key.toLowerCase()
    let value = options.value
    switch(key){
        case 'admin':
        case 'administrator':
        case 'administrador':
            if (!auth.perfilAdministrador()) {
                console.error('auth.js => O usuário não é um administrador')
                throw {route: routeLogin.name}
            }
        break

        case 'perfil':
            value = value ? value.toLowerCase() : ''
            let aliases = "admin|administrator"
            if (aliases.indexOf(value) !== -1) {
                value = 'administrador'
            }
            if (!auth.perfil(value)) {
                console.error('auth.js => O usuário não tem o perfil de "'+value+'"')
                throw {route: routeLogin.name}
            }
        break

        case 'permissao':
            if (!auth.permissao(value)) {
                console.error('auth.js => O usuário não tem a permissão de "'+value+'"')
                throw {route: routeLogin.name}
            }
        break

        default:
            console.error('auth.js => Opção inválida para a rota: ', options, route)
            throw {route: routeError.name}
        break
    }
}

const configureMiddleware = function () {
    CreateMiddleware('auth', authMiddleware)
}

configureMiddleware()

const autoCheck = function () {
    setInterval(() => {
        if (useAutoCheck &&  auth.check()) {
            auth.apiCheck()
        }
    }, autoCheckInterval)
}

export default {
    install (Vue, options) {
        
        if (!options.Vue) {
            throw 'O parâmetro "Vue" não foi passado corretamente'
        }

        if (!options.store) {
            throw 'O parâmetro "store" não foi passado corretamente'
        }

        if (!options.app) {
            throw 'O parâmetro "app" não foi passado corretamente'
        }

        if (!options.router) {
            throw 'O parâmetro "Router" não foi passado corretamente'
        }

        Vue = options.Vue
        $router = options.router
        $store = options.store
        app = options.app

        if ($store.state.app.auth) {
            autoCheckUrl = $store.state.app.auth.autocheckUrl ? $store.state.app.auth.autocheckUrl : defaultAutoCheckUrl
            useAutoCheck = !!$store.state.app.auth.autocheck
            autoCheckInterval = $store.state.app.auth.interval ? $store.state.app.auth.interval : defaultAutoCheckInterval
        }
        
        if ($store.state.app.route) {
            routeError = $store.state.app.route.error ? $store.state.app.route.error : defaultRouteError
            routeLogin = $store.state.app.route.login ? $store.state.app.route.login : defaultRouteLogin
            routeLogout = $store.state.app.route.logout ? $store.state.app.route.logout : defaultRouteLogout
        }
        
        if (auth.check()) {
            setTimeout(() => {
                auth.apiCheck()
            }, 50)
        }

        if (useAutoCheck) {
            autoCheck()
        }

        Object.defineProperties(Vue.prototype, {
            $auth: {
                get: function () {
                    return auth
                }
            }
        })
    }
}
