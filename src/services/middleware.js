let Vue
let $router
let $store
let app

let middlewares = []

let defaultRouteError = {name: 'erro'}

let routeError = defaultRouteError

export const CreateMiddleware = function (name, fn) {
    middlewares.push({name, fn})
}

let matchAuthMiddleware = function (middleware) {
    return middleware && middleware.indexOf('auth') === 0
}

const getMiddlewaresNamesFromRoute = function (route) {
    if (route.meta 
        && route.meta.middleware 
        && typeof route.meta.middleware == 'object') {
        return route.meta.middleware
    }
    console.error('Não foi configurado o middleware para a rota: ', route)
    throw {route: routeError.name}
}

const parseMiddlewareName = function (fullname) {
    let name, key, value
    let partials = fullname.split(':')
    name = partials[0]
    if (typeof partials[1] != 'undefined') {
        partials = partials[1].split(',')
        key = partials[0]
        value = partials[1] ? partials[1] : null
    }
    let options = {
        name, 
        key,
        value,
        fullname
    }

    return options
}

const executeMiddleware = function (middleware, options, route) {
    middleware.fn(options, route)
}

const executeMiddlewares = function (names, route) {
    names.forEach((name) => {
        let options = parseMiddlewareName(name)
        let middleware = middlewares.find((m) => m.name == options.name)
        if (!options) {
            console.error('Middleware "'+name+'" não configurado utilizado na a rota: ', route)
            throw {route: routeError.name}
        }
        executeMiddleware(middleware, options, route)
    })
}

const configureMiddleware = function () {
    $router.beforeEach((to, from, next) => {
        let names
        try {
            names = getMiddlewaresNamesFromRoute(to)
            executeMiddlewares(names, to)
            next()
        } catch (e) {
            if (e.route) {
                next({name: e.route})
            } else if (e.name) {
                next({name: e.name})
            } else {
                
            }
        }
    })
}

export default {
    install (Vue, options) {
        
        if (!options.Vue) {
            throw 'O parâmetro "Vue" não foi passado corretamente'
        }

        if (!options.store) {
            throw 'O parâmetro "store" não foi passado corretamente'
        }

        if (!options.app) {
            throw 'O parâmetro "app" não foi passado corretamente'
        }

        if (!options.router) {
            throw 'O parâmetro "Router" não foi passado corretamente'
        }

        Vue = options.Vue
        $router = options.router
        $store = options.store
        app = options.app

        if ($store.state.app.route) {
            routeError = $store.state.app.route.error ? $store.state.app.route.error : defaultRouteError
        }

        configureMiddleware()
    }
}
