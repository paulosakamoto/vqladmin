// Configuration for your app
 
module.exports = function (ctx) {
  return {
    dev: true,
    prod: false,  
    mode: { spa: true },
    modeName: 'spa',
    theme: { mat: true },
    themeName: 'mat',
    // preFetch: true,

    // app plugins (/src/plugins)
    plugins: [
      'serviceProvider'
    ],
    css: [
      'app.styl'
    ],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      //'material-icons',
      'fontawesome'
      // 'ionicons',
      // 'mdi',
    ],
    supportIE: false,
    vendor: {
      add: [],
      remove: []
    },
    build: {
      htmlFilename: 'app.html',
      distDir: 'public/app',
      publicPath: '/app',
      scopeHoisting: true,
      vueRouterMode: 'history',
      // env: {},
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      // useNotifier: false,
      extendWebpack (cfg) {
        // cfg.module.rules.push({
        //   enforce: 'pre',
        //   test: /\.(js|vue)$/,
        //   loader: 'eslint-loader',
        //   exclude: /(node_modules|quasar)/
        // })
      }
    },
    devServer: {
      // https: true,
      // port: 8080,
      open: false, // opens browser window automatically
      proxy: {
        '/api': 'http://localhost:8000',
        '/storage': 'http://localhost:8000',
      },
      historyApiFallback: true
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      iconSet: 'fontawesome', 
      components: [
        // 'QParallax',
        // 'QVideo',
        // 'QScrollObservable',
        // 'QInfiniteScroll',
        // 'QScrollArea',
        // 'QChatMessage',
        // 'QNoSsr',
        'QLayout',
        'QLayoutHeader',
        'QLayoutFooter',
        'QLayoutDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtnGroup',
        'QBtnDropdown',
        'QBtn',
        'QIcon',
        'QSearch',
        'QProgress',
        'QField',
        'QInput',
        'QChipsInput',
        'QColor',
        'QColorPicker',
        'QAutocomplete',
        'QEditor',
        'QSlider',
        'QRange',
        'QRadio',
        'QCheckbox',
        'QToggle',
        'QBtnToggle',
        'QOptionGroup',
        'QDatetime',
        'QDatetimePicker',
        'QSelect',
        'QRating',
        'QKnob',
        'QUploader',
        'QCard',
        'QCardTitle',
        'QCardMain',
        'QCardMedia',
        'QCardSeparator',
        'QCardActions',
        'QCollapsible',
        'QTable',
        'QTh',
        'QTr',
        'QTd',
        'QTableColumns',
        'QList',
        'QListHeader',
        'QItem',
        'QItemMain',
        'QItemSeparator',
        'QItemSide',
        'QItemTile',
        'QStepper',
        'QStep',
        'QStepperNavigation',
        'QTree',
        'QAjaxBar',
        'QModal',
        'QPopover',
        'QTooltip',
        'QPopupEdit',
        'QCarousel',
        'QCarouselSlide',
        'QCarouselControl',
        'QSlideTransition',
        'QAlert',
        'QChip',
        'QJumbotron',
        'QTimeline',
        'QTimelineEntry',
        'QPageSticky',
        'QFab',
        'QFabAction',
        'QResizeObservable',
        'QWindowResizeObservable'
      ],
      directives: [
        // 'TouchPan',
        // 'TouchSwipe',
        // 'TouchHold',
        'Ripple',
        'CloseOverlay',
        'ScrollFire',
        'BackToTop',
        'Scroll'
      ],
      // Quasar plugins
      plugins: [
        // 'AppVisibility',
        // 'AppFullscreen',

        'LocalStorage',
        'SessionStorage',
        'Notify',
        'Loading',
        'QSpinner',
        'QSpinnerAudio',
        'ActionSheet',
        'Dialog',
        // 'Cookies'
      ]
    },
    // animations: 'all' --- includes all animations
    animations: 'all',
    pwa: {
      cacheExt: 'js,html,css,ttf,eot,otf,woff,woff2,json,svg,gif,jpg,jpeg,png,wav,ogg,webm,flac,aac,mp4,mp3',
      navigateFallback: '/',
      manifest: {
        name: 'VQL Admin',
        short_name: 'VQL-Admin',
        start_url: "/",
        description: 'Painel Administrativo com VueJS 2 + Quasar Framework + Laravel 5.7',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      extendWebpack (cfg) {
        // do something with cfg
      },
      packager: {
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      }
    },

    // leave this here for Quasar CLI
    starterKit: '1.0.2'
  }
}
