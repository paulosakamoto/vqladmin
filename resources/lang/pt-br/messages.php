<?php

return [

    '404' => 'Recurso não encontrado',
    'erro' => 'Um erro inesperado ocorreu',
    'goHome' => 'Ir para a home',
    'ohNo' => 'Ah não',
    'TokenMismatchException' => 'Desculpe, sua sessão expirou. Por favor atualize a página e tente novamente.',
];
