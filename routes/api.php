<?php

use Illuminate\Http\Request;

Route::prefix('v1')->group(function(){

    Route::middleware('auth:api')->get('logout', function(Request $request){
        $user = $request->user();
        if ($user) {
            appAuditoria()->factory([
                'descricao' => 'Efetuou o logout',
                'dados' => [
                    'tokenId' => $user->token() ? $user->token()->id : null
                ],
                'entidade' => 'sistema_usuario',
                'entidade_id' => $user->id,
                'sistema_usuario_id' => $user->id,
            ]);
            if ($user->token()) {
                $request->user()->token()->revoke();
            }
        }
        
        $request->session()->invalidate();

        return jsonReponseData(null, null, true);
    });

    Route::middleware(['auth:api'])->group(function(){
        Route::get('/me', function (Request $request) {
            $user = $request->user();
            return response()->json(
                jsonReponseData([
                    'id' => $user->id,
                    'nome' => $user->nome,
                    'email' => $user->email,
                    'criado_em' => (string)$user->criado_em,
                    'atualizado_em' => (string)$user->atualizado_em,
                    'permissoes' => $user->permissoes(['sistema_permissao.nome'])->pluck('nome')
                ])
            );
        });

        Route::prefix('/test')
        ->group(function(){
            Route::get('/response', function () {
                sleep(1);
                return response()->json(jsonReponseData(sprintf('Requisição vinda do servidor feita em "%s".', date('d/m/Y H:i:s'))));
            });

            Route::post('/saved', function () {
                sleep(1);
                return response()->json(jsonReponseData(null, 'Salvo com sucesso'));
            });

            Route::get('/error', function () {
                sleep(1);
                return response()->json(jsonReponseData(null, 'Erro ao salvar', false));
            });

            Route::get('/exception', function () {
                sleep(1);
                throw new Exception('Um erro ocorreu', 1);
            });
        });
    });
    
    Route::get('csrfToken', function(){
        return response()->json(jsonReponseData(
            [
                'token' => csrf_token()
            ]
        ));
    });


    /*
    |--------------------------------------------------------------------------
    | Oauth Routes
    |--------------------------------------------------------------------------
    | 
    */
   
    //O método Passport::routes registra middlewares web e auth
    //\Laravel\Passport\Passport::routes(null, ['middleware' => ['api', 'oauthResponse']]);

    Route::prefix('oauth')
        ->namespace('\Laravel\Passport\Http\Controllers')
        //o middleware api já está registrado no \App\Providers\RouteServiceProvider
        //o middleware oauthResponse faz um parse das respostas de erro 
        //do Passport, já que até a versão 5.7 não é possível personalizar
        //a resposta, nem capturar algumas exceções
        ->middleware(['oauthResponse'])
        ->group(function(){

        /*
        |--------------------------------------------------------------------------
        | For Autorization
        |--------------------------------------------------------------------------
        | \Laravel\Passport\RouteRegistrar@forAuthorization
        */
        /*Route::middleware(['auth:api'])
            ->group(function(){
            Route::get('/authorize', 'AuthorizationController@authorize');
            Route::post('/authorize', 'ApproveAuthorizationController@approve');
            Route::delete('/authorize', 'DenyAuthorizationController@deny');
        });*/

        /*
        |--------------------------------------------------------------------------
        | For Access Tokens
        |--------------------------------------------------------------------------
        | \Laravel\Passport\RouteRegistrar@forAccessTokens
        */  
        Route::post('/token', 'AccessTokenController@issueToken')->middleware(['oauthLog:login']);
        Route::middleware(['auth:api'])
            ->group(function(){
            Route::get('/tokens', 'AuthorizedAccessTokenController@forUser');
            Route::delete('/tokens/{token_id}', 'AuthorizedAccessTokenController@destroy');
        });

        /*
        |--------------------------------------------------------------------------
        | For Transient Tokens
        |--------------------------------------------------------------------------
        | \Laravel\Passport\RouteRegistrar@forTransientTokens
        */
        //Route::middleware(['auth:api'])->post('/token/refresh', 'TransientTokenController@refresh');

        /*
        |--------------------------------------------------------------------------
        | For Clients
        |--------------------------------------------------------------------------
        | \Laravel\Passport\RouteRegistrar@forClients
        */
        /*Route::middleware(['auth:api'])
           ->group(function(){
            Route::get('/clients', 'ClientController@forUser');
            Route::post('/clients', 'ClientController@store');
            Route::put('/clients/{client_id}', 'ClientController@update');
            Route::delete('/clients/{client_id}', 'ClientController@destroy');
        });*/

        /*
        |--------------------------------------------------------------------------
        | For Personal Tokens
        |--------------------------------------------------------------------------
        | \Laravel\Passport\RouteRegistrar@forPersonalAccessTokens
        */
        /*Route::middleware(['auth:api'])
           ->group(function(){
            Route::get('/scopes', 'ScopeController@all');
            Route::get('/personal-access-tokens', 'PersonalAccessTokenController@forUser');
            Route::post('/personal-access-tokens', 'PersonalAccessTokenController@store');
            Route::delete('/personal-access-tokens/{token_id}', 'PersonalAccessTokenController@destroy');
        });*/
    });
});

//options
Route::match(['options'], '/{pattern?}', function() {
    return response('');
})->where('pattern', '[\/\w\.-]*');

//404
Route::match(['get', 'post', 'delete', 'put', 'patch'], '/{pattern?}', function() {
    throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
})->where('pattern', '[\/\w\.-]*');

