<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//options
Route::match(['options'], '/{pattern?}', function() {
    return response('');
})->where('pattern', '[\/\w\.-]*');

// Redirect all get to the front-end router
Route::get('/{pattern?}', function() {
    return view('app');
})->where('pattern', '[\/\w\.-]*');

//404
Route::match(['post', 'delete', 'put', 'patch'], '/{pattern?}', function() {
    throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
})->where('pattern', '[\/\w\.-]*');
