<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaUsuarioPerfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_usuario_perfil', function (Blueprint $table) {
            $table->unsignedInteger('sistema_perfil_id');
            $table->unsignedInteger('sistema_usuario_id');

            $table->foreign('sistema_perfil_id')
                ->references('id')
                ->on('sistema_perfil')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('sistema_usuario_id')
                ->references('id')
                ->on('sistema_usuario')
                ->onUpdate('cascade')
                ->onDelete('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sistema_usuario_perfil', function(Blueprint $table){
            $table->dropForeign('sistema_usuario_perfil_sistema_perfil_id_foreign');
            $table->dropForeign('sistema_usuario_perfil_sistema_usuario_id_foreign');
        });
        Schema::dropIfExists('sistema_usuario_perfil');
    }
}
