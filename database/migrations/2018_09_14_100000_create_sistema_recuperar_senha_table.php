<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaRecuperarSenhaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_recuperar_senha', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('token');
            $table->timestamp('criado_em')->nullable();

            $table->index('email');
            $table->index('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sistema_recuperar_senha', function(Blueprint $table){
            $table->dropIndex('sistema_recuperar_senha_email_index');
            $table->dropIndex('sistema_recuperar_senha_token_index');
        });
        Schema::dropIfExists('sistema_recuperar_senha');
    }
}
