<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_log', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedInteger('sistema_usuario_id')->nullable();
            $table->string('entidade')->nullable();
            $table->unsignedInteger('entidade_id')->nullable();
            $table->enum('tipo', ['Log', 'Auditoria'])->default('Log');
            $table->date('data');
            $table->time('hora');
            $table->string('ip', 32);
            $table->string('descricao', 255);
            $table->json('dados');
            $table->timestamp('criado_em');

            $table->index(['tipo', 'data']);
            $table->index(['tipo', 'sistema_usuario_id', 'data']);
            $table->index(['entidade_id', 'entidade']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sistema_log', function(Blueprint $table){
            $table->dropIndex('sistema_log_tipo_data_index');
            $table->dropIndex('sistema_log_tipo_sistema_usuario_id_data_index');
            $table->dropIndex('sistema_log_entidade_id_entidade_index');
        });
        Schema::dropIfExists('sistema_log');
    }
}
