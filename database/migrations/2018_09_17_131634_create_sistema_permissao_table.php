<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaPermissaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_permissao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('descricao', 100)->nullable();
            
            $table->enum('ativo', ['Sim', 'Não'])->default('Sim');
            $table->timestamp('criado_em')->nullable();
            $table->timestamp('atualizado_em')->nullable();
            $table->timestamp('excluido_em')->nullable();

            $table->unique(['nome']);
            $table->index(['ativo', 'nome']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sistema_permissao', function(Blueprint $table){
            $table->dropIndex('sistema_permissao_ativo_nome_index');
            $table->dropUnique('sistema_permissao_nome_unique');
        });
        Schema::dropIfExists('sistema_permissao');
    }
}
