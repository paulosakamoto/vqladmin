<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaPerfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 50);
            $table->string('descricao', 100)->nullable();

            $table->enum('ativo', ['Sim', 'Não'])->default('Sim');
            $table->timestamp('criado_em')->nullable();
            $table->timestamp('atualizado_em')->nullable();
            $table->timestamp('excluido_em')->nullable();

            $table->unique(['nome']);
            $table->index(['ativo', 'nome']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sistema_perfil', function(Blueprint $table){
            $table->dropIndex('sistema_perfil_ativo_nome_index');
            $table->dropUnique('sistema_perfil_nome_unique');
        });
        Schema::dropIfExists('sistema_perfil');
    }
}
