<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SistemaUsuario;

class CreateSistemaUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('senha');
            $table->string('token_senha')->nullable();
            $table->enum('ativo', SistemaUsuario::enumAtivo())->default(SistemaUsuario::ATIVO_SIM);
            $table->timestamp('criado_em')->nullable();
            $table->timestamp('atualizado_em')->nullable();
            $table->timestamp('excluido_em')->nullable();

            $table->unique(['email']);
            $table->index(['ativo', 'email']);
            $table->index(['ativo', 'nome']);
            $table->index(['ativo', 'token_senha']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sistema_usuario', function(Blueprint $table){
            $table->dropIndex('sistema_usuario_ativo_email_index');
            $table->dropIndex('sistema_usuario_ativo_nome_index');
            $table->dropIndex('sistema_usuario_ativo_token_senha_index');
            $table->dropUnique('sistema_usuario_email_unique');
        });
        Schema::dropIfExists('sistema_usuario');
    }
}
