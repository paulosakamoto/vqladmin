<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemaPerfilPermissaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_perfil_permissao', function (Blueprint $table) {
            $table->unsignedInteger('sistema_perfil_id');
            $table->unsignedInteger('sistema_permissao_id');

            $table->foreign('sistema_perfil_id')
                ->references('id')
                ->on('sistema_perfil')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('sistema_permissao_id')
                ->references('id')
                ->on('sistema_permissao')
                ->onUpdate('cascade')
                ->onDelete('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sistema_perfil_permissao', function(Blueprint $table){
            $table->dropForeign('sistema_perfil_permissao_sistema_perfil_id_foreign');
            $table->dropForeign('sistema_perfil_permissao_sistema_permissao_id_foreign');
        });
        Schema::dropIfExists('sistema_perfil_permissao');
    }
}
