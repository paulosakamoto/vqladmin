<?php

use Illuminate\Database\Seeder;
use App\SistemaPermissao;

class SistemaPermissaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (SistemaPermissao::first()) {
            return;
        }

        $permissoes = [
            ['nome' => 'perfil.administrador', 'descricao' => 'Perfil de administrador'],
        ];

        foreach ($permissoes as $attr) {
            $permissao = SistemaPermissao::forceCreate($attr);
            appAuditoria()->factory([
                'dados' => $attr,
                'descricao' => 'Permissão criada',
                'entidade' => 'sistema_permissao',
                'entidade_id' => $permissao->id,
            ]);
        }
    }
}
