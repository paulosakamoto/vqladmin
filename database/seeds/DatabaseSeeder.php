<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SistemaPermissaoSeeder::class);
        $this->call(SistemaPerfilSeeder::class);
        $this->call(SistemaUsuarioSeeder::class);
    }
}
