<?php

use Illuminate\Database\Seeder;
use App\SistemaPermissao;
use App\SistemaPerfil;

class SistemaPerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (SistemaPerfil::first()) {
            return;
        }

        $perfis = [
            [
                'nome' => 'administrador', 
                'descricao' => 'Perfil de administrador',
                'permissoes' => [
                    'perfil.administrador'
                ]
            ],
        ];

        foreach ($perfis as $attrs) {
            $perfil = SistemaPerfil::forceCreate([
                'nome' => $attrs['nome'],
                'descricao' => $attrs['descricao'],
            ]);
            if (!empty($attrs['descricao'])) {
                $permissoes = SistemaPermissao::whereIn('nome', $attrs['permissoes'])->get();
                $ids = $permissoes->pluck('id');
                $perfil->permissoes()->attach($ids);
            }
            appAuditoria()->factory([
                'dados' => $attrs,
                'descricao' => 'Perfil criado',
                'entidade' => 'sistema_perfil',
                'entidade_id' => $perfil->id,
            ]);
        }
    }
}
