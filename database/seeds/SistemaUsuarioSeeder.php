<?php

use Illuminate\Database\Seeder;
use App\SistemaUsuario;
use App\SistemaPerfil;

class SistemaUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (SistemaUsuario::first()) {
            return;
        }

        $users = [
            [
                'nome' => 'Administrador',
                'email' => 'admin@example.com',
                'senha' => '123456',
                'perfils' => ['administrador'],
            ]
        ];
        
        foreach ($users as $attrs) {
            $user = SistemaUsuario::forceCreate([
                'nome' => $attrs['nome'],
                'email' => $attrs['email'],
                'senha' => $attrs['senha']
            ]);
            
            if (!empty($attrs['perfils'])) {
                $perfils = SistemaPerfil::whereIn('nome', $attrs['perfils']);
                $ids = $perfils->pluck('id');
                $user->perfis()->attach($ids);
            }
            appAuditoria()->factory([
                'dados' => $attrs,
                'descricao' => 'Usuário criado',
                'entidade' => 'sistema_usuario',
                'entidade_id' => $user->id,
            ]);
        }
    }
}
